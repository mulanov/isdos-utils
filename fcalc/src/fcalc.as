       ORG  24000

VARS   EQU  23627
STKBOT EQU  23651
STKEND EQU  23653
RAMTOP EQU  23730
E_LINE EQU  23641
WORKSP EQU  23649

;подготовка среды
       CALL $INROM
       LD   IY,23610
       LD   HL,vars
       LD   (VARS),HL
       LD   HL,ERSP
       LD   (23613),HL
       LD   BC,ESP
       LD   (HL),C
       INC  HL
       LD   (HL),B
       LD   HL,e_line
       LD   (E_LINE),HL
       JR   1$
       DEFB "U","n","C","o",10
2$     DEFB 7,7,7,7,7,7*8,7,7,7,7
3$     DEFW INFO+4,WINDQ+4,WINDAN+4,WIW+4,WIW2+4,WIW2-10,QUIT+4,WIERSM+4
       DEFW ERRWIND+4,EXERWI+4
1$     LD   IX,2$
       LD   HL,3$
       LD   B,10
4$     LD   E,(HL)
       INC  HL
       LD   D,(HL)
       INC  HL
       LD   A,(IX)
       INC  IX
       LD   (DE),A
       DJNZ 4$
;сохранение распределения памяти
;и распределение памяти
       LD   C,#10
       RST  16
       EXX
       INC  HL
       INC  HL
       INC  HL
       LD   BC,EXPBUF+770
       LD   (MEMAD),HL
       LD   E,(HL)
       LD   (HL),C
       INC  HL
       LD   D,(HL)
       LD   (HL),B
       LD   (UTOP),DE
       INC  HL
       INC  HL
       INC  HL
       LD   A,(HL)
       LD   (CASHES),A
       LD   L,65
       LD   C,0
CRCS   DEC  L
       LD   A,L
       CP   5
       JP   Z,EXIT1
       RST  16
       JR   C,CRCS

;разборка с ключами
       LD   IX,BYTE
       LD   (IX),0
PARM   LD   C,#43
       RST  16
       RET  C
       JP   NZ,PART1
       AND  A
       JR   NZ,KEY
       EXX
       PUSH HL
       LD   DE,INFILE
       LD   BC,11
       LDIR
       POP  HL
       LD   DE,OUTFIL
       LD   C,8
       LDIR
       EXX
       INC  (IX)
       JR   PARM
KEY    SET  5,A
       CP   "n"
       LD   HL,FLAG
       JR   NZ,KEY1
       RES  TRIG,(HL)
       JR   PARM
KEY1   CP   "o"
       JR   NZ,PARM
       SET  OBZ,(HL)
       JR   PARM
NOFILE LD   IX,INFO
       LD   C,#61
       XOR  A
       RST  16
       LD   C,#66
       RST  16
       LD   C,7
       RST  16
       RES  5,A
       CP   "H"
       JP   NZ,EXIT1
       LD   HL,HELFIL
       LD   (HELSP+1),HL
       JP   PART3

PART1

       LD   A,(IX)
       AND  A
       JR   Z,NOFILE
       LD   C,#36
       LD   IX,IOPATH
       XOR  A
       RST  16
       RET  C
       LD   C,#45
       RST  16
       EXX
       LD   BC,#FFF7
       ADD  HL,BC
       PUSH HL
       POP  IX
       XOR  A
       LD   C,#37
       RST  16

       LD   C,#36
       XOR  A
       LD   IX,EXPPAT
       RST  16
       RET  C
       CALL INPAT
       LD   HL,OUTFIL
       LD   C,#24
       RST  16
       RET  C
       DEC  C
       RST  16
       RET  C
       LD   C,#25
       RST  16
       RET  C
       LD   HL,0
       LD   A,#3
       CALL WRIT1

       CALL OPIN
       RET  C
       LD   HL,0
       LD   (PASS),HL
PART2
       LD   A,(FLAG)
       AND  #FC
       LD   (FLAG),A
       LD   HL,0
       LD   (SM),HL
       LD   (EXPR),HL
CI4    LD   HL,FLAG
       RES  TREB,(HL)
       LD   HL,(SM)
       LD   IX,VARBUF
CI3    CALL READ1
       LD   (BB5),HL
       INC  HL
       LD   (SM),HL
       CP   3
       JP   Z,READY1
       CP   "*"
       JP   Z,YET11
       CP   " "
       JR   Z,CI3
       JR   C,CI3
       BIT  5,A
       JR   Z,WW1
       LD   DE,(PASS)
       LD   A,D
       OR   E
       JP   NZ,YET11
WW1    INC  IX
CINE   CALL READ1
       INC  HL
       CP   ":"
       JP   Z,DWOET
       CP   "="
       JR   Z,WW4
       CP   " "
       JR   Z,CINE
       JR   C,CINE
       CP   "0"
       JP   C,ERR87
       CP "9"+1
       JR C,NUMBER
       CP "A"
       JP C,ERR87
       CP "Z"+1
       JR C,NORM
       CP "a"
       JP C,ERR87
       CP "z"+1
       JP NC,ERR87
NORM   INC IX
       LD A,(IX)
       CP #FF
       JP Z,ERR87
       JR CINE
NUMBER INC IX
       LD A,(IX-2)
       CP #FF
       JP Z,ERR87
       JR CINE
WW4    LD IX,EXPBUF
CI2    CALL READ1
       INC HL
       CP #20
       JR C,CI2 ;изменить, если предполагается использовать символьные и строковые термы
       INC IX
       CP ";"
       JR Z,WW2
       CP ":"
       JR NZ,CI2
       LD A,(FLAG)
       SET TREB,A
       LD (FLAG),A
WW2    LD (SM),HL
       LD HL,STANER
       LD (ERAD),HL
DW3    LD DE,EXPBUF
       LD (IX-1),13
       PUSH DE
       PUSH IX
       POP HL
       SBC HL,DE
       LD B,H
       LD C,L
       PUSH BC
       LD HL,FLAG
       BIT TRIG,(HL)
       CALL NZ,PREOBR
GOTOW  LD HL,ramtop
       LD (23730),HL
       LD HL,stkbot
       LD (23651),HL
       LD HL,stkend
       LD (23653),HL
       LD HL,worksp
       LD (23649),HL
       POP BC
       POP DE
       LD (STACK1),SP
       CALL CALC
       CALL ADDVAR ;добавить переменную
       LD   HL,(BB5)
       CALL READ1
       SET  5,A
       CALL WRIT1
       LD   HL,FLAG
       SET  CALD,(HL)
       BIT  TREB,(HL)
       JR   NZ,TONO
       BIT  OBZ,(HL)
       JP   Z,YET
       LD   HL,(SM)
       DEC  HL
       JR   OBQZ
TONO   LD   HL,(SM)
OBQZ   LD   IX,EXPBUF
WYW1   CALL READ1
       INC  HL
       INC  IX
       CP   ";"
       JR   NZ,WYW1
WYW2   LD   (SM),HL
       DEC  IX

       LD   A,":"
       LD   (IX),A
       INC  IX

       LD   HL,VARBUF
VP1    LD   A,(HL)
       LD   (IX),A
       INC  HL
       INC  IX
       CP   "="
       JR   Z,VP2
       CP   ":"
       JR   NZ,VP1
       LD   (IX-1),"="
VP2    LD   HL,(E_LINE)
       LD   DE,6
       SBC  HL,DE
       LD   A,(HL)
       INC  HL
       LD   E,(HL)
       INC  HL
       LD   D,(HL)
       INC  HL
       LD   C,(HL)
       INC  HL
       LD   B,(HL)
       INC  HL
       PUSH HL
       PUSH AF
       CALL $ROM
       CALL #2AB1
       CALL $CHIC
       POP  AF
       LD   HL,(STKEND)
       LD   DE,5
       AND  A
       SBC  HL,DE
       LD   (HL),A
       CALL $ROM
       RST  #28
       DEFB #2E,#38
       CALL #2BF1
       CALL $CHIC
       LD   B,C
WYW100 LD   A,(DE)
       LD   (IX),A
       INC  DE
       INC  IX
       DJNZ WYW100

       PUSH IX
       POP  HL
       LD   (HL),#0D
       INC  HL
       LD   DE,EXPBUF
       AND  A
       SBC  HL,DE
       EX   DE,HL
       PUSH HL
       PUSH DE
       CALL OPOUT
       POP  DE
       PUSH DE
       LD   C,#31
       RST  16
       POP  DE
       RET  C
       POP  IX
       XOR  A
       LD   HL,(OM)
       LD   C,#2A
       RST  16
       RET  C
       ADD  HL,DE
       LD   (OM),HL
       CALL OPIN
       JR   YET
YET11
       LD   HL,(SM) ;*
       LD   IX,BYTE
YET12  CALL READ1
       INC  HL
       CP   ";"
       JR   NZ,YET12
       LD   (SM),HL
YET    LD   HL,(EXPR)
       INC  HL
       LD   (EXPR),HL
       JP   CI4


READY1 LD   HL,(PASS)
       INC  HL
       LD   (PASS),HL
       LD   HL,FLAG
       BIT  ERD,(HL)
       JP   Z,PART3
       BIT  CALD,(HL)
       JP   NZ,PART2
ERR83  LD   A,28
       JP   ERROR
ERR87  LD   A,32
       JR   ERR83+2
ERREXL LD   A,1
       JR   ERR83+2
ERE3   LD   A,2
       JR   ERR83+2
ERR4   LD   A,3
       JR   ERR83+2

; преобрезование выражения,
;находящегося в буфере выражения
PREOBR CALL $ROM
       LD HL,FLAG
       SET ZAP,(HL)
PRE    LD A,(DE)
       CP #D
       JP Z,$CHIC
       INC DE
       CALL PEREM ;C,если запрещенный
       JR NC,PRE1
       CALL SETZAP ;установка бита ZAP FLAG, если C
       JR PRE
PRE1
       LD HL,FLAG
       BIT ZAP,(HL)
       PUSH AF
       OR A
       CALL SETZAP
       POP AF
       JR Z,PRE
       DEC DE
       PUSH DE
       LD (LINE),DE
       CALL FIND
       EX DE,HL
       POP DE
       INC DE
       JR NC,PRE
       DEC DE
       LD (DE),A
       INC DE
       LD BC,256
       LDIR
       LD DE,(LINE)
       INC DE
       JR PRE
PEREM  CP "0"
       JR C,ZZAP
       CP "9"+1
       JR C,NZAP
       CP "A"
       JR C,ZZAP
       CP "Z"+1
       JR C,NZAP
       CP "a"
       JR C,ZZAP
       CP "z"+1
       JR C,NZAP
ZZAP   SCF
       RET
NZAP   OR A
       RET
SETZAP LD HL,FLAG
       RES ZAP,(HL)
       RET NC
       SET ZAP,(HL)
       RET
FIND   LD   HL,#96
       LD   B,#A5
7$     LD   DE,(LINE)
       LD   (8$+1),HL
3$     LD   A,(DE)
       AND  %01011111
1$     LD   C,(HL)
       RES  7,C
       CP   C
       JR   NZ,2$
       INC  HL
       INC  DE
       LD   A,(DE)
       CP   "A"
       JR   NC,3$
       DEC  HL
       LD   A,(HL)
       CP   #80
       JR   C,2$
       SCF
       JR   4$
2$     INC  B
       JR   Z,5$
8$     LD   HL,0
6$     LD   A,(HL)
       AND  #80
       INC  HL
       JR   Z,6$
       JR   7$
5$     OR   A
4$     LD   A,B
       RET

;обработка двоеточия
DWOET  LD   B,0
       LD   (SM),HL
DW10   LD   IX,EXPBUF
DW1    CALL READ1
       INC  HL
       CP   #0D
       JR   Z,DW1
       INC  IX
       CP   "("
       JR   NZ,DW11
       CP   B
       JP   NZ,SKOBA
DW11   CP   ";"
       JR   NZ,DW1
       LD   (SS),HL
       LD   (SIX),IX
       LD   A,(EXPBUF)
       AND  %00100000
       JR   NZ,DWW3
       LD   (IX-1),"="
       PUSH IX
       POP  HL
       LD   DE,EXPBUF-1
       LD   (FETT1+1),DE
  ;     INC  DE
       SBC  HL,DE
       LD   A,L
       LD   (LENA),A
       CALL EXPAT
       RET  C
       LD   HL,EXPL
       LD   C,#25
       RST  16
       RET  C
       CALL FF
       JR   C,DWW1
       LD   IX,EXPBUF
DWW2
       CALL READ1
       INC  HL
       INC  IX
       CP   " "
       JR   C,DWW2
       CP   ";"
       JR   NZ,DWW2
       LD   HL,ERR4
       LD   (ERAD),HL
       PUSH IX
       CALL INPAT
       POP  IX
       RET  C
       CALL OPIN
       RET  C
       LD   HL,(SS)
       LD   (SM),HL
       JP   DW3
DWW1   CALL INPAT
       RET  C
       CALL OPIN
       RET  C
DWW3   LD   HL,FLAG
       LD   IX,(SIX)
       SET  TREB,(HL)
       LD   (IX-1),"?"
       LD   (IX),#03
       LD   HL,WINDQ
       LD   DE,EXPBUF-8
       LD   BC,8
       LDIR
       LD   IX,EXPBUF-8
       LD   C,#61
       XOR  A
       RST  16
       LD   C,#66
       RST  16
       LD   HL,EXPBUF+256
       LD   (HL),#0D
       DEC  HL
       LD   DE,EXPBUF+254
       LD   BC,256
       LD   (HL),#20
       LDDR
DW2
       LD   IX,WINDAN
       LD   C,#61
       XOR  A
       RST  16
       LD   HL,#0101
       LD   C,#6B
       RST  16
       LD   HL,EXPBUF
       LD   A,40
       LD   BC,#066E
       LD   DE,#FF00
       RST  16
       RET  C
       JR   Z,DDWW
       CALL PEXIT
       JR   DW2
DDWW   LD   D,0
       LD   E,A
       ADD  HL,DE
       LD   (CRSP),HL
       INC  HL
       PUSH HL
       LD   HL,EXPBUF
       LD   DE,EXPBUF+512
       LD   BC,256
       LDIR
       POP  IX
       LD   HL,ERDW
       LD   (ERAD),HL
       JP   DW3

;открытие каталогов вх. файла и explain
INPAT  LD   IX,IOPATH
       JR   EXPAT1
EXPAT  LD   IX,EXPPAT
EXPAT1 XOR  A
       LD   C,#37
       RST  16
       RET

SKOBA
       LD   (SS),HL
       LD   (SIX),IX
       LD   (IX-1),":"
       LD   DE,EXPBUF-1
       PUSH IX
       POP  HL
       SBC  HL,DE
       LD   A,L
       LD   (LENA),A
 ;      DEC  DE
       LD   (FETT1+1),DE
       CALL EXPAT
       LD   HL,EXPL
       LD   C,#25
       RST  16
       RET  C
       CALL FF
       JR   NC,FND3
       CALL INPAT
       RET  C
       CALL OPIN
       RET  C
       LD   HL,(SS)
       LD   IX,(SIX)
       LD   (IX-1),"("
       JP   DW1

;поиск в открытом файле строки((FETT1+1))
;длиной (LENA) байт
FF
       LD   HL,0
       LD   A,#D
       LD   (EXPBUF-1),A
       LD   IX,BYTE
FETT1  LD   DE,EXPBUF
m6     CALL READ1
       INC  HL
       EX   DE,HL
       CP   (HL)
       EX   DE,HL
       JR   Z,m4
       CP   3
       JR   NZ,m6
m9     SCF
       RET
m4     CALL READ1
       JR   C,m9
       CP   3
       JR   Z,m9
       PUSH HL
       INC  DE
       LD   A,(LENA)
       LD   B,A
m2     LD   A,(DE)
       INC  DE
       DJNZ m1
       POP  BC
       RET
m1     CP   #21
       JR   C,m2
       CP   #E0
       JR   C,m7
       SUB  #30
m7     AND  %11011111
       LD   C,A
m3     PUSH BC
       CALL READ1
       POP  BC
       INC  HL
       CP   #21
       JR   C,m3
       CP   #E0
       JR   C,m8
       SUB  #30
m8     AND  %11011111
       CP   C
       JR   Z,m2
       POP  HL
       JR   FETT1

FND3   LD   IX,PATH
FND2   CALL READ1
       INC  IX
       INC  HL
       CP   ";"
       JR   NZ,FND2
       LD   (IX-1),#D
       CALL INPAT
       RET  C
       CALL OPIN
       RET  C
       LD   IX,(SIX)
       LD   HL,(SS)
NEXR   CALL READ1
       INC  HL
       INC  IX
       CP   ")"
       JR   NZ,NEXR
       LD   (IX-1),":"
       PUSH IX
       POP  HL
       LD   DE,(SIX)
       SBC  HL,DE
       LD   A,L
       INC  A
       LD   (LENA),A
       LD   HL,PATH
       LD   C,#49
       RST  16
       CALL EXPAT
       RET  C
       LD   C,#40
       RST  16
       JP   C,ERREXL
       JP   NZ,ERREXL
       LD   HL,(SIX)
       DEC  HL
       LD   (HL),#D
       LD   (FETT1+1),HL
       LD   HL,0
       CALL FF
       JR   C,WOPROS
REH2   LD   IX,EXPBUF
REH    CALL READ1
       INC  HL
       INC  IX
       CP   " "
       JR   C,REH
       CP   ";"
       JR   NZ,REH
       LD   HL,ERE3
       LD   (ERAD),HL
       LD   HL,FLAG        ;*
       SET  TREB,(HL)
       PUSH IX
       CALL INPAT
       POP  IX
       RET  C
       CALL OPIN
       RET  C
       JP   DW3

WOPROS
       LD   IX,WIW
       LD   C,#61
       XOR  A
       RST  16
       LD   HL,(SIX)
       PUSH HL
       LD   BC,1000
       LD   A,":"
       CPIR
       LD   (HL),3
       DEC  HL
       LD   (HL),"?"
       POP  HL
       LD   C,#67
       RST  16
       LD   IX,WIW2
       LD   C,#61
       XOR  A
       RST  16
       LD   HL,EXPBUF
       LD   (SIX),HL
       LD   HL,PUPBUF
       LD   (PUP),HL
       LD   HL,CSRBUF
       LD   (PCSR),HL
       LD   HL,0
       LD   B,17
       CALL WIW34
       LD   HL,(PUPBUF)
       LD   (FIRPUP),HL
       LD   IX,WIW2
MENU   LD   C,#91
       RST  16
       CALL INPAT
       RET  C
       CALL OPIN
       RET  C
       LD   B,"("
       LD   HL,(SM)
       JP   DW10
PEXIT  PUSH IX
       LD   IX,QUIT
       LD   C,#61
       XOR  A
       RST  16
       LD   C,#66
       RST  16
       LD   C,7
       RST  16
       CP   "Y"
       JR   Z,EXXIT
       CP   "y"
       POP  IX
       RET  NZ
EXXIT  JP   EXIT1

EE     LD   A,(IX-9)
       DEC  A
       LD   L,A
       LD   H,0
       ADD  HL,HL
       LD   DE,PUPBUF
       ADD  HL,DE
       LD   E,(HL)
       INC  HL
       LD   D,(HL)
       EX   DE,HL
       JP   REH2

QA     LD   C,9
       RST  16
       JR   Z,QA
       CP   "q"
       JR   Z,PSCUP
       CP   "a"
       JR   Z,PSCDN
RRR    CP   A
       RET

PSCDN
       LD   A,(IX-9)
       CP   17
       JR   NZ,RRR
       DEC  A
       LD   (IX-9),A
       LD   A,(FLAG)
       BIT  MORE,A
       RET  Z
       LD   DE,PUPBUF
       LD   HL,PUPBUF+2
       LD   BC,32
       LDIR
       LD   (PUP),DE
       LD   HL,EXPBUF
       PUSH HL
       LD   BC,768
       PUSH BC
       LD   A,#D
       CPIR
       POP  BC
       POP  DE
       LDIR
       LD   HL,EXPBUF      ;*
       LD   BC,768
       LD   A,3
       CPIR
       DEC  HL
  ;     LD   (HL),#D
       LD   (SIX),HL
       LD   HL,(PCSR)
       DEC  HL
       LD   (PCSR),HL
       LD   HL,(PUPBUF+30)
       LD   B,1
       CALL WIW34

       LD   HL,EXPBUF
       LD   IX,WIW2
       LD   C,#67
       RST  16
       CP   A
       RET

PSCUP  LD   A,(IX-9)
       CP   1
       JR   NZ,RRR
       INC  A
       LD   (IX-9),A
       LD   HL,(PUPBUF)
       LD   DE,(FIRPUP)
       SBC  HL,DE
       RET  Z
       LD   HL,PUPBUF+31
       LD   DE,PUPBUF+33
       LD   BC,32
       LDDR
       LD   HL,EXPBUF+768-42
       LD   DE,EXPBUF+768
       LD   BC,768-41
       LDDR
       LD   HL,(PUPBUF)
       DEC  HL
       LD   IX,BYTE
R11    DEC  HL
       CALL READ1
       CP   ":"
       JR   NZ,R11
       INC  HL
       LD   (PUPBUF),HL
R12    DEC  HL
       CALL READ1
       CP   #D
       JR   NZ,R12
       INC  HL
       LD   E,0
       LD   B,41
       LD   IX,EXPBUF
R13
       CALL READ1
       INC  HL
       INC  IX
       INC  E
       CP   ":"
       JR   Z,R14
       DJNZ R13
R14    LD   (IX-1),#D
       LD   C,E
       LD   B,0
       LD   HL,EXPBUF
       PUSH HL
       ADD  HL,BC
       LD   DE,EXPBUF+42
       EX   DE,HL
       LD   BC,768-41
       LDIR
       POP  HL
       LD   IX,WIW2
       LD   C,#67
       RST  16
       LD   HL,FLAG
       SET  MORE,(HL)
       CP   A
       RET


WIW34
       LD   A,(FLAG)
       RES  MORE,A
       LD   (FLAG),A
WIW33  LD   IX,(SIX)
WIW3   CALL READ1
       INC  HL
       CP   #3
       JR   Z,ENDW
       CP   #D
       JR   NZ,WIW3
WIW4   CALL READ1
       INC  HL
       INC  IX
       CP   3
       JR   Z,ENDW
       CP   ":"
       JR   Z,WIW5
       JR   WIW4
WIW5   ;LD   (SIX),IX
       LD   (IX-1),#0D
       EX   DE,HL
       LD   HL,(PUP)
       LD   (HL),E
       INC  HL
       LD   (HL),D
       INC  HL
       LD   (PUP),HL
       LD   HL,(PCSR)
       LD   (HL),1
       INC  HL
       LD   (PCSR),HL
       EX   DE,HL
       DJNZ WIW3
       LD   A,(FLAG)
       SET  MORE,A
       LD   (FLAG),A
ENDW   LD   A,(IX-1)
       CP   3
       RET  Z
       LD   (IX),3
       LD   IX,BYTE
ENDW1  CALL READ1
       INC  HL
       CP   ";"
       JR   NZ,ENDW1
       INC  HL
       CALL READ1
       CP   3
       RET  NZ
       LD   HL,FLAG
       RES  MORE,(HL)
       RET


;добавление переменной в область переменных
ADDVAR LD HL,(E_LINE)
       DEC HL
       LD IX,VARBUF
       LD A,(IX+1)
       CP "="
       PUSH AF
       CP ":"
       PUSH AF
       POP BC
       LD A,C
       POP BC
       OR C
       BIT 6,A
       LD A,(IX)
       JR NZ,ONESYM
       AND  #1F
       OR   %10100000
       LD (HL),A
       INC HL
       INC IX
AV1    LD A,(IX)
       LD (HL),A
       INC IX
       CP "="
       JR Z,AV4
       CP ":"
       JR Z,AV4
       INC HL
       JR AV1
AV4    DEC HL
       SET 7,(HL)
       JR AV2

ONESYM AND  #1F
       OR   %01100000
       LD   (HL),A
AV2    INC  HL
       CALL $ROM
       PUSH HL
       CALL #2BF1
       POP  HL
       LD   (HL),A
       INC  HL
       LD   (HL),E
       INC  HL
       LD   (HL),D
       INC  HL
       LD   (HL),C
       INC  HL
       LD   (HL),B
       INC  HL
       LD   (HL),#80
       INC  HL
       LD   (E_LINE),HL
       CALL $CHIC
       RET

;запись и чтение байта из открытого файла
WRIT1  LD   C,#2A
       PUSH DE
       PUSH IX
       LD   DE,1
       LD   IX,BYTE
       LD   (IX),A
       XOR  A
       RST  16
       POP  IX
       POP  DE
       RET
READ1  LD   C,#29
       PUSH DE
       LD   DE,1
       XOR  A
       RST  16
       POP  DE
       LD   A,(IX)
       RET

;откр. вх. файла
OPIN   LD   HL,INFILE
OPIN1  LD   C,#25
       RST  16
       RET
;откр. вых. файла
OPOUT  LD   HL,OUTFIL
       JR   OPIN1


YET1   LD   HL,FLAG
       SET  ERD,(HL)
       LD HL,(BB5)
       LD IX,BYTE
       CALL READ1
       RES 5,A
       CALL WRIT1
       LD HL,(SM)
       DEC HL
YET2   CALL READ1
       INC HL
       CP ";"
       JR NZ,YET2
       LD (SM),HL
       JP   YET


PART3  CALL NORMM
       LD   C,#73
       RST  16
HELSP  LD   HL,OUTFIL
       LD   DE,EXFILE
       LD   C,#4F
       RST  16
       LD   HL,EXLINE
       LD   C,#49
       RST  16
       LD   C,#93
       RST  16
       RET  C
;восстановление распределения памяти
NORMM
       LD   C,2
       RST  16
       LD   DE,(UTOP)
       LD   HL,(MEMAD)
       LD   (HL),E
       INC  HL
       LD   (HL),D
       LD   A,(CASHES)
       LD   C,0
       RST  16
       RET

;обработка ошибок
ERDW   LD IX,WIERSM
       XOR A
       LD C,#61
       RST 16
       LD HL,#0100
       LD C,#6B
       RST 16
       LD HL,ERRORM
       LD C,#6D
       LD B,6
       RST 16
       LD C,#7
       RST 16
       LD   HL,EXPBUF+512
       LD   DE,EXPBUF
       LD   BC,256
       LDIR
       LD HL,(CRSP)
       LD (HL),#20
       JP DW2
ESPCH  LD   SP,(#DD6D)
       POP  HL
       POP  HL
       POP  HL
       INC  SP
       POP  HL
       POP  HL
       LD   A,#10
       LD   BC,#1FFD
       OUT  (C),A
       LD   A,(HL)
       PUSH AF
       CALL $CHIC
       POP  AF
       JR   ESP1
ESP    LD A,(23610)
ESP1   LD SP,(STACK1)
       SCF
       LD   HL,(ERAD)
       JP   (HL)

STANER INC A
       CP 2
       JP Z,YET1

ERROR
       LD BC,#0A7C
       LD E,A
       LD (ERC+1),A
       LD D,0
       LD HL,SPACE
       AND A
       LD A,3
       RST 16
       LD DE,(EXPR)
       INC DE
       LD HL,SPACE2
       AND A
       LD A,5
       LD BC,#0A7C
       RST 16
       LD A,13
       LD (SPACE2+5),A
       LD IX,ERRWIND
       XOR A
       LD C,#61
       RST 16
       LD HL,#080A
       LD C,#C
       RST 16
       LD HL,ERRORM
       LD C,#6C
       RST 16
       LD HL,#0A0A
       LD C,#C
       RST 16
       LD HL,POQSN
       LD C,#6C
       RST 16
       LD C,7
       RST 16
       CP   "m"
       JR   NZ,EXIT1
       CALL EXPAT
       LD   C,#45
       RST  16
       EXX
       LD   BC,#FFF7
       ADD  HL,BC
       PUSH HL
       POP  IX
       XOR  A
       LD   C,#37
       RST  16
       LD   HL,FILEMS
       LD   C,#25
       RST  16
       JR   C,EXIT1
ERC    LD A,0
       ADD A,"0"
       CP "9"+1
       JR C,ERCC
       ADD A,"A"-"9"-1
ERCC   LD (ERCD+1),A
       LD HL,0
ERRED  CALL READ1
       INC HL
ERCD   CP 0
       JR NZ,ERRED
       LD IX,EXPBUF
ERRED1 CALL READ1
       INC HL
       INC IX
       CP 3
       JR Z,EXIT1
       CP #D
       JR NZ,ERRED1
       LD DE,EXPBUF
       PUSH IX
       POP HL
       SBC HL,DE
       LD B,L
       DEC B
       LD IX,EXERWI
       XOR A
       LD C,#61
       RST 16
       LD HL,EXPBUF
       LD A,%11000001
       LD C,#68
       LD E,1
       RST 16

;выход
       LD   C,7
       RST  16
EXIT1  CALL NORMM
       LD   C,#80
       RST  16

;вычисление выражения. Ответ в стеке.
;DE-начало
;BC-длина
CALC   CALL $ROM
       XOR A
       CALL #2AB1
       LD B,#1D
       RST #28
       DEFB #3B,#38
       CALL $CHIC
       RET
;НАСТРОЙКА ПРОЦЕДУР $ROM & $CHIC
$INROM LD   HL,0
       LD   E,(HL)
       LD   A,(HL)
       INC  A
       INC  (HL)
       CP   (HL)
       LD   (HL),E
       RET  Z
       LD   A,#C9
       LD   ($ROM),A
       LD   ($CHIC),A
       RET
;ПОДКЛЮЧЕНИЕ ПЗУ БЕЙСИКА, DI
$ROM   DI
       PUSH BC
       PUSH HL
       LD   HL,(#E2B5)
       LD   (Q1+1),HL
       LD   HL,(#DD6D)
       LD   (Q2+1),HL
       LD   HL,(#DDF7)
       LD   (Q3+1),HL
       LD   HL,(#C064)
       LD   (Q4+1),HL
       LD   HL,(#C001)
       LD   (Q5+1),HL
       LD   HL,#FFFF
       LD   (#C064),HL
       LD   HL,ESPCH
       LD   (#C064),HL
       LD   BC,#1FFD
       LD   A,#10
       OUT  (C),A
       LD   (23610),A
       POP  HL
       POP  BC
       RET
;ПОДКЛЮЧЕНИЕ СИСТЕМЫ IS-DOS CHIC, EI
$CHIC  PUSH BC
       LD   BC,#1FFD
       LD   A,#11
       OUT  (C),A
       PUSH HL
Q1     LD   HL,0
       LD   (#E2B5),HL
Q2     LD   HL,0
       LD   (#DD6D),HL
Q3     LD   HL,0
       LD   (#DDF7),HL
Q4     LD   HL,0
       LD   (#C064),HL
Q5     LD   HL,0
       LD   (#C001),HL
       POP  HL
       POP  BC
       EI
       RET


WIW    DEFB 0,0,5,32,6*8+#40,#80,1,41
       DEFB 6*8+#40,#02,6*8+#40,1
       DEFW 0
       DEFW QA
       DEFW 0
       DEFW EN
WIW2   DEFB 1,5,19,30,6+#40,#80,2,38
       DEFW CSRBUF-1
       DEFW EXPBUF
QUIT   DEFB 10,11,3,12,6+#40,#80,15,11
       DEFM "Quit? (y/n)"
       DEFB 3
WINDQ  DEFB 0,0,8,32,7*8,#80,1,41
WINDAN DEFB 0,9,8,32,7,#80,1,41
WIERSM DEFB 12,11,3,7,%01010111,3,18
ERRWIN DEFB 6,#07,5,22,%01010111,3
EXERWI DEFB 1,13,3,30,%01110000,3,2,39
FILEMS DEFM "fcalc   msg"
POQSN  DEFM "Нажмите [М] для пояснения"
       DEFB #0D
       DEFW 0
ERSP   DEFW 0
       DEFW 0
BYTE   DEFB 0
FLAG   DEFB #10
CALD   EQU  0
ERD    EQU  1
TREB   EQU  2
ZAP    EQU  3
TRIG   EQU  4
MORE   EQU  5
OBZ    EQU  6
LINE   DEFW 0
PCSR   DEFW 0 ;указатель в CSRBUF
SS     DEFW 0 ;промеж. смещ. от нач. вх. файла
LENA   DEFW 0 ;длина строки А
SIX    DEFW 0 ;промежуточное знач. IX
CRSP   DEFW 0 ;адрес символа #0D при обработке двоеточия
ERAD   DEFW 0 ;адрес перехода по ошибке
BB5    DEFW 0 ;смещение первого символа выражения
EXPR   DEFW 0 ;номер выражения
PASS   DEFW 0 ;номер прохода
STACK1 DEFW 0 ;копия регистра SP6
SM     DEFW 0 ;смещение от начала вх. файла
OM     DEFW 0 ;смещ. от начала вых. файла
CASHES DEFB 0 ;бывший размер кэша
MEMAD  DEFW 0 ;адрес UTOP'a
UTOP   DEFW 0 ;бывшее значение UTOP
PUP    DEFW 0 ;указатель в PUPBUF
FIRPUP DEFW 0
       DEFB #FF
VARBUF DEFS 11 ;буфер для имени переменной
       DEFB #FF
CSRBUF DEFS 17*2
EN     DEFW EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE,EE
INFILE DEFS 11
OUTFIL DEFM "outtext txt"
       DEFB 1,0,0,1,0
       DEFS 16
EXLINE DEFM "Q:SHELL\tv "
EXFILE DEFM "ABCDEFGH.txt "
EXPL   DEFM "explain txt"
PUPBUF         ;буфер для смещений пунктов
PATH   DEFS 64 ;буфер для пути файла-таблицы
EXPPAT DEFS 5 ;буферы для сохр. среды
IOPATH DEFS 5
ERRORM DEFM "Ошибка"
SPACE  DEFS 3
       DEFM " в выражении"
SPACE2 DEFS 6
INFO   DEFB 5,5,8,32-10,%1110000,3,9,25
       DEFM " Formula Calculator v1.1"
       DEFB #D,#D
       DEFM "   (c) 1996 MegaProduct"
       DEFB #D
       DEFM "  Written by MIHA ULANOV"
       DEFB #D,#D
       DEFM "    Press [H] to help"
       DEFB #3
HELFIL DEFM "fcalc   hlp"

errsp  DEFW ESP

vars
       DEFB #80
e_line
       DEFB #80
worksp EQU  e_line+200
stkbot EQU  worksp+50
stkend EQU  stkbot
ramtop EQU  stkend+800
EXPBUF EQU  ramtop+1
